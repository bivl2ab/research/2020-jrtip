# 2020-JRTIP

# **A compact and recursive Riemannian motion descriptor for untrimmed activity recognition**

### _Fabio Martínez $^{1}$, Michèle Gouiffès $^{2}$, Gustavo Garzón $^{1}$, Antoine Manzanera $^{3}$_

1. Biomedical Imaging, Vision and Learning Laboratory (BIVL2ab). Universidad Industrial de Santander (UIS), Colombia. E-mail: {famarcar, gustavo.garzon}@saber.uis.edu.co
2. LIMSI, CNRS, Université Paris-Saclay, FRANCE. E-mail: michele.gouiffes@limsi.f
3. U2IS/Robotics \& Autonomous Systems, ENSTA Paris, Institut Polytechnique de Paris, FRANCE. E-mail: antoine.manzanera@ensta-paris.fr

A very low dimension frame-level motion descriptor is herein proposed with the capability to represent incomplete dynamics, thus allowing online action prediction. At each frame, a set of local trajectory kinematic cues are spatially pooled using a covariance matrix. The set of frame-level covariance matrices forms a Riemannian manifold that describes motion patterns. A set of statistic measures are computed over this manifold to characterize the sequence dynamics, either globally, or instantaneously from a motion history. Regarding the Riemannian metrics, two different versions are proposed: (1) by considering tangent projections with respect to updated recursive statistics, and (2) by mapping the covariance onto a linear matrix using as reference the identity matrix. The proposed approach was evaluated for two different tasks: (1) for action classification on complete video sequences and (2) for online action recognition, in which the activity is predicted at each frame. The method was evaluated using two public datasets: KTH and UT-Interaction. For action classification, the method achieved an average accuracy of 92.27 and 81.67\%, for KTH and UT-interaction, respectively. In partial recognition task, the proposed method achieved similar classification rate as for the whole sequence by using only the 40 and 70\% on KTH and UT sequences, respectively. 

Please cite this work as: [xxx]
The original paper is cited at (here xxx)[here]

